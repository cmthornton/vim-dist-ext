" dist-ext.vim - Treat *.*.dist files as if they didn't have a .dist extension
"
" Maintainer:   Chris Thornton <http://cmthornton.com> <chris@cmthornton.com>
" Repositories: http://git.cmthornton.com/vim-dist-ext.git
"               https://bitbucket.org/cmthornton/vim-dist-ext.git
" Version:      0.0.1
"


let s:script_name = 'dist_ext'


" Guard against multiple includes.
let s:guard = 'g:loaded_'.s:script_name

if exists(s:guard)
  finish
else
  let {s:guard} = 1
endif



" Returns the subextension of a filename.
"
" @example s:subextension("hello.ext.dist") "=> `ext`
"
" @param filename
"
" @return string
"
function! s:subextension(filename)
  return matchstr(a:filename, '\c[^\.]*\.\zs[^\.]\+\ze\.dist$')
endfunction



" Returns the filename without the `.dist` extension.
"
" @example s:filename_nodist("hello.ext.dist") "=> `hello.ext`
"
" @param filename
"
" @return string
"
function! s:filename_nodist(filename)
  return matchstr(a:filename, '\c\zs[^\.]*\.[^\.]\+\ze\.dist$')
endfunction



" Calls the autocmds for a `*.*.dist` file, as if the file did not have the
" `.dist` extension.
"
" @note Side effect: triggers autocommands
"
" @param filename
"
function! s:invoke_subext_autocmds(filename)
  let subext = s:subextension(a:filename)
  let name_nodist = s:filename_nodist(a:filename)

  " If we couldn't get a subextension, then stop before triggering the
  " autocmds for a given filename.
  if empty(subext)
    return
  endif

  execute "doau BufNewFile,BufRead ".name_nodist
endfunction


autocmd BufNewFile,BufRead *.dist call s:invoke_subext_autocmds(@%)
