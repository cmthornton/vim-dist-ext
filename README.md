# dist-ext.vim
`dist-ext.vim` automatically triggers autocommands for `*.*.dist` files, as if
they didn't have the `.dist` extension.


## How It Works
If the filename is `config.yml.dist`, then this plugin will trigger the
autocommands for `yml` extensions. Triggering the autocommands for `yml` file
extensions will (in most cases) set the filetype to `yml`, turn on syntax
highlighting, or whatever else you've configured `vim` to do with `yml`
extensions.

If the filename has a `.dist` extension but does not have a subextension, then
this plugin will not trigger any autocommands.


## Installation
You can install `dist-ext.vim` using [Pathogen][pathogen], [Vundle][vundle], or manually.


### Pathogen
To install `dist-ext.vim` using [Pathogen][pathogen]:

1. Install [Pathogen][pathogen].
1. Clone this repository to `"$HOME"/.vim/bundle/`.

        cd "$HOME"/.vim/bundle
        git clone https://bitbucket.org/cmthornton/vim-dist-ext.git

### Vundle
To install `dist-ext.vim` using [Vundle][vundle]:

1. Install [Vundle][vundle].

1. Add the following line to your `.vimrc`:

        Bundle 'https://bitbucket.org/cmthornton/vim-dist-ext.git'

1. Save your `.vimrc`.

1. Update your bundles:

    * From inside of vim:

            :BundleInstall

    * or from the commandline:

            vim +BundleInstall +qa


### Manually
If you go this route, you're on your own.


## Legal
Distributed under the terms of the Vim license, see `:help license`.

Copyright (C) Chris Thornton.


[pathogen]: https://github.com/tpope/vim-pathogen
[vundle]: https://github.com/gmarik/vundle
